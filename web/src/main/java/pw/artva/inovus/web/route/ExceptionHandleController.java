package pw.artva.inovus.web.route;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * Global exception handler.
 *
 * @author Artur Vakhrameev
 */
@ControllerAdvice
public class ExceptionHandleController {

    private static final Logger logger = LoggerFactory.getLogger(ExceptionHandleController.class);
    @Autowired
    private MessageSource messageSource;

    /**
     * Handles global exception
     *
     * @param e exception
     * @return redirect url
     */
    @ExceptionHandler({Exception.class, RuntimeException.class})
    public String handleException(Exception e) {
        logger.error(messageSource.getMessage("error", null, LocaleContextHolder.getLocale()), e);

        return "redirect:/error";
    }
}

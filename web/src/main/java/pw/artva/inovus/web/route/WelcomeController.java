package pw.artva.inovus.web.route;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import pw.artva.inovus.common.date.WelcomeDayTime;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.time.LocalTime;

/**
 * Welcome page controller.
 *
 * @author Artur Vakhrameev
 */
@Controller
public class WelcomeController {

    /**
     * Get welcome page
     *
     * @param principal current user
     * @param model     current ui model
     * @return updated ui model
     */
    @GetMapping("/welcome")
    public Model welcome(Principal principal, Model model) {
        model.addAttribute("welcomeDayTime", WelcomeDayTime.getByLocalTime(LocalTime.now()));
        model.addAttribute("username", principal.getName());

        return model;
    }

    /**
     * Sends logout request and redirects to sign-in page.
     *
     * @param request http request
     * @return redirect url
     * @throws ServletException errors during logout process
     */
    @PostMapping("/welcome")
    public String logout(HttpServletRequest request) throws ServletException {
        request.logout();
        return "redirect:/sign-in?logout";
    }

}

package pw.artva.inovus.web;

import org.springframework.context.annotation.*;

/**
 * Application configuration class.
 *
 * @author Artur Vakhrameev
 */
@Configuration
@ComponentScan(basePackages = {"pw.artva.inovus"},
        //exclude web configuration
        excludeFilters = {@ComponentScan.Filter(type = FilterType.ASPECTJ, pattern = "pw.artva.inovus.web.route.*")})
public class ApplicationConfig {

}

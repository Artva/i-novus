package pw.artva.inovus.web.route;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import pw.artva.inovus.common.user.UserRegistration;
import pw.artva.inovus.common.user.UserRegistrationValidator;
import pw.artva.inovus.common.user.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;

/**
 * User registration controller.
 *
 * @author Artur Vakhrameev
 */
@Controller
public class SignUpController {

    @Autowired
    private UserRegistrationValidator userRegistrationValidator;
    @Autowired
    private UserService userService;

    @InitBinder
    protected void initBinder(WebDataBinder dataBinder) {
        //add form validator
        dataBinder.setValidator(userRegistrationValidator);
    }

    /**
     * Get new registration form model
     *
     * @param model ui model
     * @return updated ui model
     */
    @GetMapping("/sign-up")
    public Model newRegistration(Model model) {
        model.addAttribute("userReg", new UserRegistration());

        return model;
    }

    /**
     * Sign-up new user.
     *
     * @param userReg user registration model
     * @param errors  validation errors
     * @param request http request
     * @return url
     * @throws ServletException errors during login process
     */
    @PostMapping("/sign-up")
    public String signUp(@ModelAttribute("userReg") @Valid UserRegistration userReg,
                         Errors errors, Principal principal, HttpServletRequest request) throws ServletException {
        if (errors.hasErrors()) {
            return "sign-up";
        } else {
            userService.addNewUser(userReg);
            if (principal == null) {
                request.login(userReg.getUsername(), userReg.getPassword());
            }

            return "redirect:/welcome";
        }
    }
}

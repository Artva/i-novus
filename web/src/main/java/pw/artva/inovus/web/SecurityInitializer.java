package pw.artva.inovus.web;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Spring web user initializer.
 *
 * @author Artur Vakhrameev
 */
public class SecurityInitializer extends AbstractSecurityWebApplicationInitializer {
}

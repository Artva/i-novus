package pw.artva.inovus.web.route;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import pw.artva.inovus.common.user.UserModel;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * User sign-in page controller
 *
 * @author Artur Vakhrameev
 */
@Controller
public class SignInController {

    @Autowired
    private MessageSource messageSource;

    @PostMapping("/sign-in")
    public String processLogin(@ModelAttribute("user") @Valid UserModel user,
                               Errors errors,
                               HttpServletRequest request) throws ServletException {

        if (errors.hasErrors()) {
            errors.reject("sign-in.error.required");
            return "sign-in";
        } else {
            request.login(user.getUsername(), user.getPassword());
            return "redirect:/welcome";
        }
    }

    /**
     * Get sign-in form
     *
     * @param logout show logout message if not null
     * @param model  ui model
     * @return updated ui model
     */
    @GetMapping("/sign-in")
    public Model signInForm(@RequestParam(name = "logout", required = false) String logout, Model model) {
        model.addAttribute("user", new UserModel());
        if (logout != null) {
            model.addAttribute("logout", getMessage("sign-in.logout"));
        }

        return model;
    }

    /**
     * Handle {@link ServletException} during authentication process.
     *
     * @param model ui model
     * @return view with authenication error
     */
    @ExceptionHandler(ServletException.class)
    public Model handleAuthException(Model model) {
        model.addAttribute("authError", getMessage("sign-in.error.auth"));
        model.addAttribute("user", new UserModel());

        return model;
    }

    private String getMessage(String code) {
        return messageSource.getMessage(code, null, LocaleContextHolder.getLocale());
    }

}

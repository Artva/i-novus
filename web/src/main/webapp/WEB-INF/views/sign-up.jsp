<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <link href="<c:url value="/resources/bootstrap.min.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/main.css" />" rel="stylesheet">
    <title><spring:message code="reg.title"/></title>
</head>
<body>
<div>
    <form:form id="regForm" method="POST" modelAttribute="userReg">
        <form:errors cssClass="error"/>
        <div class="form-horizontal">
            <div class="form-group">
                <form:label path="username" cssClass="col-sm-2 control-label"><spring:message code="reg.username"/></form:label>
                <div class="col-sm-4">
                    <form:input cssClass="form-control" type="text" path="username"/>
                </div>
            </div>

            <div class="form-group">
                <form:label path="password" cssClass="col-sm-2 control-label"><spring:message code="reg.password"/></form:label>
                <div class="col-sm-4">
                    <form:input cssClass="form-control" type="password" path="password"/>
                </div>
            </div>

            <div class="form-group">
                <form:label path="passwordRepeat" cssClass="col-sm-2 control-label"><spring:message code="reg.password.repeat"/></form:label>
                <div class="col-sm-4">
                    <form:input cssClass="form-control" type="password" path="passwordRepeat"/>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-4">
                    <input class="btn btn-primary" name="submit" type="submit"
                           value="<spring:message code="reg.submit"/>"/>
                </div>
            </div>
        </div>
    </form:form>
</div>
</body>
</html>
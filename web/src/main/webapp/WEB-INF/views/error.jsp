<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <link href="<c:url value="/resources/bootstrap.min.css" />" rel="stylesheet">
    <title><spring:message code="error.title"/></title>
</head>
<body>
<div class="alert alert-warning">
    <spring:message code="error"/>
</div>
</body>
</html>
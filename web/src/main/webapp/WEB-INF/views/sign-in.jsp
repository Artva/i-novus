<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <link href="<c:url value="/resources/bootstrap.min.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/main.css" />" rel="stylesheet">
    <title><spring:message code="sign-in.title"/></title>
</head>
<body>
<div>
    <form:form id="formLogin" method="POST" modelAttribute="user">
        <form:errors cssClass="warn"/>
        <div class="error">${authError}</div>
        <div class="message">${logout}</div>
        <div class="form-horizontal">
            <div class="form-group">
                <form:label cssClass="col-sm-2 control-label" path="username"><spring:message
                        code="sign-in.username"/></form:label>
                <div class="col-sm-4">
                    <form:input cssClass="form-control" type="text" path="username"/>
                </div>
                <a class="col-sm-2" href="<c:url value="/sign-up"/>"><spring:message code="sign-in.sign-up"/> </a>
            </div>

            <div class="form-group">
                <form:label cssClass="col-sm-2 control-label" path="password"><spring:message code="sign-in.password"/>
                </form:label>
                <div class="col-sm-4">
                    <form:input cssClass="form-control" type="password" path="password"/>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-4">
                    <input class="btn btn-primary" name="submit" type="submit"
                           value="<spring:message code="sign-in.submit"/>"/>
                </div>
            </div>
        </div>
    </form:form>
</div>
</body>
</html>
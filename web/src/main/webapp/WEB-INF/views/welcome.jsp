<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <link href="<c:url value="/resources/bootstrap.min.css" />" rel="stylesheet">
    <title><spring:message code="welcome.title"/></title>
</head>
<body>
<div class="form-inline">
    <div class="form-group">
        <div>
            <spring:message code="${welcomeDayTime.messageKey}"/>, ${username}!
        </div>
    </div>
    <form:form method="post" class="form-group">
        <div class="col-sm-2">
            <input class="btn btn-default" type="submit" value="<spring:message code="welcome.submit"/>">
        </div>
    </form:form>
</div>
</body>
</html>
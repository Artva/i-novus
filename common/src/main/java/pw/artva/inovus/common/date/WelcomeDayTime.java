package pw.artva.inovus.common.date;

import java.time.LocalTime;
import java.util.stream.Stream;

import static java.time.LocalTime.*;

/**
 * Time period of a day.
 *
 * @author Artur Vakhrameev
 */
public enum WelcomeDayTime {
    /**
     * Morning time
     */
    MORNING(of(6, 0), of(10, 0), "welcome.morning"),
    /**
     * Day time
     */
    DAY(of(10, 0), of(18, 0), "welcome.day"),
    /**
     * Evening time
     */
    EVENING(of(18, 0), of(22, 0), "welcome.evening"),
    /**
     * Night time before midnight
     */
    BEFORE_MIDNIGHT(of(22, 0), MAX, "welcome.night"),

    /**
     * Night time after midnight
     */
    AFTER_MIDNIGHT(MIDNIGHT, of(6, 0), "welcome.night");

    WelcomeDayTime(LocalTime from, LocalTime to, String messageKey) {
        this.from = from;
        this.to = to;
        this.messageKey = messageKey;
    }

    private final LocalTime from;
    private final LocalTime to;
    private final String messageKey;

    /**
     * @param localTime time for check
     * @return true if assignable to this time
     */
    public boolean assignableTo(LocalTime localTime) {
        return (from.equals(localTime) || from.isBefore(localTime)) && to.isAfter(localTime);
    }

    /**
     * @param localTime
     * @return day time assignable to this time
     */
    public static WelcomeDayTime getByLocalTime(final LocalTime localTime) {
        return Stream.of(values()).filter(welcomeDayTime -> welcomeDayTime.assignableTo(localTime))
                .findAny()
                .orElseThrow(IllegalArgumentException::new);
    }

    /**
     * @return welcome message key
     */
    public String getMessageKey() {
        return messageKey;
    }
}

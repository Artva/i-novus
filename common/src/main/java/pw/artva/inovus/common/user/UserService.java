package pw.artva.inovus.common.user;

import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * Security service for user authentication and registration.
 *
 * @author Artur Vakhrameev
 */
public interface UserService extends UserDetailsService {

    /**
     * Checks username existing in storage.
     *
     * @param username
     * @return true if exists
     */
    boolean checkUserExists(String username);

    /**
     * Saves  new user to storage
     *
     * @param userRegistration new user
     */
    void addNewUser(UserRegistration userRegistration);
}

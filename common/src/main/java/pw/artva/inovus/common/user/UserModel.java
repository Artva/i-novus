package pw.artva.inovus.common.user;


import javax.validation.constraints.NotEmpty;

/**
 * User sign-in model
 *
 * @author Artur Vakhrameev
 */
public class UserModel {

    @NotEmpty(message = "user name required")
    private String username;
    @NotEmpty(message = "password required")
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

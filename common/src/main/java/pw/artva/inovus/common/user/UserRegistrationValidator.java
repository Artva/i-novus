package pw.artva.inovus.common.user;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * User sign-up form validator
 *
 * @author Artur Vakhrameev
 */
@Component
public class UserRegistrationValidator implements Validator {

    @Autowired
    private UserService userService;

    @Override
    public boolean supports(Class<?> clazz) {
        return UserRegistration.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        UserRegistration reg = (UserRegistration) target;
        validateName(reg.getUsername(), errors);
        validatePassword(reg.getPassword(), reg.getPasswordRepeat(), errors);
    }

    private void validateName(String username, Errors errors) {
        if (StringUtils.isEmpty(username) || !isValidName(username)) {
            errors.reject("reg.error.username");
        } else if (userService.checkUserExists(username)) {
            errors.reject("reg.error.exists");
        }
    }

    private boolean isValidName(String username) {
        return username.trim().matches("[A-z\\d]{4,}");
    }

    private void validatePassword(String password, String passwordRepeat, Errors errors) {

        if (StringUtils.isEmpty(password) || !isValidPassword(password)) {
            errors.reject("reg.error.password");
        } else if (!StringUtils.equals(password, passwordRepeat)) {
            errors.reject("reg.error.password.match");
        }
    }

    private boolean isValidPassword(String password) {
        //contains digits
        return password.length() >= 8 &&
                password.matches(".*\\d+.*") &&
                //contains upper keys character
                password.matches(".*[\\p{Upper}].*+") &&
                //contains lower keys character
                password.matches(".*[\\p{Lower}].*+");

    }
}

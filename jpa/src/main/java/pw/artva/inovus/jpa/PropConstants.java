package pw.artva.inovus.jpa;

/**
 * Provides jpa properties' names.
 *
 * @author Artur Vakhrameev
 */
public interface PropConstants {

    /**
     * Property source location.
     */
    String SOURCE = "classpath:jpa.properties";

    //hibernate and jpa
    String JPA_DB_PLATFORM = "jpa.platform";
    String JPA_SCAN_PACKAGES = "jpa.scan.entity";
    String JPA_UNIT_NAME = "jpa.unit";
    String JPA_SHOW_SQL = "jpa.showsql";
    String JPA_DATABASE = "jpa.db";
    String HBM2DDL_AUTO = "hibernate.hbm2ddl.auto";

    //database
    String DATABASE_CHANGELOG = "db.changelog";
}

package pw.artva.inovus.jpa;

import liquibase.integration.spring.SpringLiquibase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.orm.jpa.JpaDialect;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaDialect;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;
import java.util.stream.Stream;

import static pw.artva.inovus.jpa.PropConstants.*;

/**
 * Java persistence api configuration.
 *
 * @author Artur Vakhrameev
 */
@Configuration
@PropertySource(SOURCE)
@EnableTransactionManagement
public class PersistenceConfig {

    @Autowired
    private Environment env;

    /**
     * @return datasource
     */
    @Bean("dataSource")
    public DataSource dataSource() {
        return new EmbeddedDatabaseBuilder()
                .generateUniqueName(true)
                .setType(EmbeddedDatabaseType.HSQL)
                .build();
    }

    /**
     * Enable auto update database schema by changelogs on app start
     *
     * @return liquibase bean
     */
    @Bean
    public SpringLiquibase springLiquibase() {
        SpringLiquibase liquibase = new SpringLiquibase();
        liquibase.setDataSource(dataSource());
        liquibase.setChangeLog(property(DATABASE_CHANGELOG));

        return liquibase;
    }

    /**
     * @return spring jpa transaction manager
     */
    @Bean
    public PlatformTransactionManager transactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager(entityManagerFactory().getObject());
        transactionManager.setDataSource(dataSource());
        transactionManager.setJpaDialect(jpaDialect());

        return transactionManager;
    }

    /**
     * @return jpa vendor adapter
     */
    @Bean
    public JpaVendorAdapter jpaVendorAdapter() {
        HibernateJpaVendorAdapter jpaVendorAdapter = new HibernateJpaVendorAdapter();
        jpaVendorAdapter.setDatabase(Database.valueOf(property(JPA_DATABASE)));
        jpaVendorAdapter.setShowSql(Boolean.parseBoolean(property(JPA_SHOW_SQL)));
        jpaVendorAdapter.setDatabasePlatform(property(JPA_DB_PLATFORM));

        return jpaVendorAdapter;
    }

    /**
     * @return dialect using for jpa
     */
    @Bean
    public JpaDialect jpaDialect() {
        return new HibernateJpaDialect();
    }

    /**
     * @return entity manager factory
     */
    @Bean
    @DependsOn("springLiquibase")
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean entityManagerFactory = new LocalContainerEntityManagerFactoryBean();
        entityManagerFactory.setDataSource(dataSource());
        entityManagerFactory.setPackagesToScan(property(JPA_SCAN_PACKAGES));
        entityManagerFactory.setPersistenceUnitName(property(JPA_UNIT_NAME));
        entityManagerFactory.setJpaVendorAdapter(jpaVendorAdapter());
        entityManagerFactory.setJpaDialect(jpaDialect());
        entityManagerFactory.setJpaProperties(hibernateProperties());

        return entityManagerFactory;
    }

    private Properties hibernateProperties() {
        Properties props = new Properties();
        addProperties(props, HBM2DDL_AUTO);

        return props;
    }

    private String property(String propName) {
        return env.getProperty(propName);
    }

    private void addProperties(Properties properties, String... propNames) {
        Stream.of(propNames)
                .forEach(propName -> properties.setProperty(propName, property(propName)));
    }
}


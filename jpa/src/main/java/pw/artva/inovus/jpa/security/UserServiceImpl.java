package pw.artva.inovus.jpa.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pw.artva.inovus.common.user.UserRegistration;
import pw.artva.inovus.common.user.UserService;
import pw.artva.inovus.jpa.dao.UserDao;
import pw.artva.inovus.jpa.entity.User;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Artur Vakhrameev
 * @see UserService
 */
@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User entity = userDao.findByName(username);
        return new org.springframework.security.core.userdetails.User(
                entity.getName(),
                entity.getPassword(),
                //every registered user has only one authority
                //so we don't store authorities in database
                getSimpleAuthorities()
        );
    }

    @Override
    public boolean checkUserExists(String username) {
        return userDao.checkUserExists(username);
    }

    @Override
    public void addNewUser(UserRegistration userRegistration) {
        userDao.save(getEntityForSignUp(userRegistration));
    }

    private User getEntityForSignUp(UserRegistration userRegistration) {
        User result = new User();
        result.setName(userRegistration.getUsername());
        //password must be encoded
        result.setPassword(passwordEncoder.encode(userRegistration.getPassword()));

        return result;
    }

    private List<GrantedAuthority> getSimpleAuthorities() {
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority("USER"));

        return authorities;
    }

}

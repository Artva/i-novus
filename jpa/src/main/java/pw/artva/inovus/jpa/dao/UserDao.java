package pw.artva.inovus.jpa.dao;

import org.springframework.stereotype.Repository;
import pw.artva.inovus.jpa.entity.User;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * User entities' jpa repository.
 *
 * @author Artur Vakhrameev
 */
@Repository
public class UserDao {

    @PersistenceContext
    private EntityManager entityManager;

    /**
     * @param name user name
     * @return found user entity
     * @throws javax.persistence.NoResultException if there isn't user with such name
     */
    public User findByName(String name) {
        return entityManager.createQuery("select u from User u " +
                "where name=:name", User.class)
                .setParameter("name", name)
                .getSingleResult();
    }

    /**
     * @param name user name
     * @return true if user with such name exists
     */
    public boolean checkUserExists(String name) {
        return entityManager.createQuery("select count(u) from User u " +
                "where name=:name", Long.class)
                .setParameter("name", name)
                .getSingleResult() > 0;
    }

    /**
     * Save new user to database
     *
     * @param user user entity
     */
    public void save(User user) {
        entityManager.persist(user);
    }
}

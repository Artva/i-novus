package pw.artva.inovus.jpa.entity;

import javax.persistence.*;

/**
 * User jpa entity.
 *
 * @author Artur Vakhrameev
 */
@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue
    @Column(name = "id", nullable = false)
    private Long id;
    @Basic
    @Column(name = "name", nullable = false, unique = true)
    private String name;
    @Basic
    @Column(name = "password", nullable = false)
    private String password;

    /**
     * @return id (primary key)
     */
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return unique user name
     */
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return user password
     */
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
